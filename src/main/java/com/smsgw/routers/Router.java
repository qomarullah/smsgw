package com.smsgw.routers;

/**
 * Bismillahirrahmanirrahim
 * @author Qomarullah
 * @time 9:27:44 PM
 */

import org.jsmpp.InvalidResponseException;
import org.jsmpp.PDUException;
import org.jsmpp.SMPPConstant;
import org.jsmpp.bean.Address;
import org.jsmpp.bean.AlertNotification;
import org.jsmpp.bean.Alphabet;
import org.jsmpp.bean.BindType;
import org.jsmpp.bean.DataSm;
import org.jsmpp.bean.DeliverSm;
import org.jsmpp.bean.DeliveryReceipt;
import org.jsmpp.bean.ESMClass;
import org.jsmpp.bean.GeneralDataCoding;
import org.jsmpp.bean.MessageClass;
import org.jsmpp.bean.MessageType;
import org.jsmpp.bean.NumberingPlanIndicator;
import org.jsmpp.bean.RegisteredDelivery;
import org.jsmpp.bean.ReplaceIfPresentFlag;
import org.jsmpp.bean.SMSCDeliveryReceipt;
import org.jsmpp.bean.SubmitMultiResult;
import org.jsmpp.bean.TypeOfNumber;
import org.jsmpp.examples.gateway.Gateway;
import org.jsmpp.extra.NegativeResponseException;
import org.jsmpp.extra.ProcessRequestException;
import org.jsmpp.extra.ResponseTimeoutException;
import org.jsmpp.session.BindParameter;
import org.jsmpp.session.DataSmResult;
import org.jsmpp.session.MessageReceiverListener;
import org.jsmpp.session.SMPPSession;
import org.jsmpp.session.Session;
import org.jsmpp.util.AbsoluteTimeFormatter;
import org.jsmpp.util.InvalidDeliveryReceiptException;
import org.jsmpp.util.TimeFormatter;

import static spark.Spark.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smsgw.App;
import com.smsgw.AutoReconnectGateway;
import com.smsgw.models.Result;
import com.smsgw.testing.AppSMPPSession;
import com.smsgw.utils.Configs;
import com.smsgw.utils.LogTDR;
import com.smsgw.utils.Utils;

import spark.Filter;
import spark.Request;
import spark.Response;
import spark.Spark;

import java.net.URLDecoder;
import java.util.*;

public class Router {

	private final static Logger log = LogManager.getLogger(Router.class);
	private static final HashMap<String, String> corsHeaders = new HashMap<String, String>();
	static {
		corsHeaders.put("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
		corsHeaders.put("Access-Control-Allow-Origin", "*");
		corsHeaders.put("Access-Control-Allow-Headers",
				"Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,");
		corsHeaders.put("Access-Control-Allow-Credentials", "true");
	}
	private static final TimeFormatter TIME_FORMATTER = new AbsoluteTimeFormatter();

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public final static void apply() {
		Filter filter = new Filter() {
			@Override
			public void handle(Request request, Response response) throws Exception {
				corsHeaders.forEach((key, value) -> {
					// response.header(key, value);
				});
				response.type("application/json");
			}
		};
		Spark.after(filter);
	}

	public Router(int port, int minThreads, int maxThreads, int timeOutMillis, Gateway gateway) {

		Spark.port(port);
		threadPool(maxThreads, minThreads, timeOutMillis);
		Spark.get("/", (req, res) -> "OK");

		// Spark.get("/sms/submit", (request, response) -> {
		Spark.get("cgi-bin/sendsms", (request, response) -> {
			return handlerSubmitSMS(request, response, gateway);
		});
		apply();
	}

	public String handlerSubmitSMS(Request request, Response response, Gateway gateway) throws JsonProcessingException {

		long start = System.currentTimeMillis();
		String resp = "";
		String err = "";
		Result result = new Result();
		result.setStatus("NOK");
		String from = request.queryParams("from");
		String to = request.queryParams("to");
		String systemtype = App.prop.getProperty("smpp.systemtype", "CMT");
		String text = request.queryParams("text");

		try {
			text = java.net.URLDecoder.decode(text, "UTF-8");
		} catch (Exception e) {
			// not going to happen - value came from JDK's own StandardCharsets
		}

		// curl
		// "http://localhost:25001/cgi-bin/sendsms?from=Google&to=6281327532224&text=TEST+JSMPP+TELIN&dlr-url=http%3A%2F%2F127.0.0.1%3A10000%2Fdlr%3Ftrx_id%3D%25F%26status%3D%25d%26err%3D%25A%26final%3D1%26tambahan%3D%25I%26internalid%3DTWIL210401185055277006%26Pbesar%3D%25P%26Pkecil%3D%25p%26Obesar%3D%25O%26Okecil%3D%25o"
		// http://127.0.0.1:10000/dlr?trx_id=%F&status=%d&err=%A&final=1&tambahan=%I&internalid=TWIL210401185055277006&Pbesar=%P&Pkecil=%p&Obesar=%O&Okecil=%o"

		String dlrurl = request.queryParams("dlr-url");
		String internalId = "";
		try {
			// dlrurl = java.net.URLDecoder.decode(dlrurl, "UTF-8");
			String[] dlrParam = dlrurl.split("internalid=");
			if (dlrParam.length > 0) {
				String[] dlrParam0 = dlrParam[1].split("&");
				if (dlrParam0.length > 0) {
					internalId = dlrParam0[0];
				}
			}
		} catch (Exception e) {
			log.error("Error get internalid", e);
			result.setMessage("internalid not found");
			resp = result.getStatus() + "|" + result.getMessage() + "|" + result.getMessageId();
			return resp;
		}

		if (from.equals("") || to.equals("")) {
			result.setMessage("Invalid Parameter");
			resp = result.getStatus() + "|" + result.getMessage() + "|" + result.getMessageId();
			return resp;
		}
		// String trxid=Utils.generateTxid("MT", to);
		/*
		 * String text = request.queryParams("text"); if(from.equals("")||to.equals(""))
		 * { result.setMessage("Invalid Parameter"); } String
		 * trxId=Utils.generateTxid("MT", to); result.setTrxId(trxId);
		 */

		String messageId = "";
		try {

			/*
			 * messageId = gateway.submitShortMessage("CMT", TypeOfNumber.INTERNATIONAL,
			 * NumberingPlanIndicator.UNKNOWN, from, TypeOfNumber.INTERNATIONAL,
			 * NumberingPlanIndicator.UNKNOWN, to, new ESMClass(), (byte) 0, (byte) 1,
			 * TIME_FORMATTER.format(new Date()), null, new
			 * RegisteredDelivery(SMSCDeliveryReceipt.SUCCESS_FAILURE), (byte) 0, new
			 * GeneralDataCoding(Alphabet.ALPHA_DEFAULT, MessageClass.CLASS1, false), (byte)
			 * 0, "jSMPP simplify SMPP on Java platform".getBytes());
			 */

			messageId = gateway.submitShortMessage(systemtype, TypeOfNumber.INTERNATIONAL,
					NumberingPlanIndicator.UNKNOWN, from, TypeOfNumber.INTERNATIONAL, NumberingPlanIndicator.UNKNOWN,
					to, new ESMClass(), (byte) 0, (byte) 1, TIME_FORMATTER.format(new Date()), null,
					new RegisteredDelivery(SMSCDeliveryReceipt.SUCCESS_FAILURE), (byte) 0,
					new GeneralDataCoding(Alphabet.ALPHA_DEFAULT, MessageClass.CLASS1, false), (byte) 0,
					text.getBytes());

			result.setStatus("OK");
			result.setMessage("Sent.");
			result.setMessageId(messageId);

			// log.info("Message submitted, message_id is {}", messageId);
		} catch (PDUException e) {
			// Invalid PDU parameter
			result.setMessage("error:" + e.getMessage());
			log.error("Invalid PDU parameter", e);
		} catch (ResponseTimeoutException e) {
			// Response timeout
			result.setMessage("error:" + e.getMessage());
			log.error("Response timeout", e);
		} catch (InvalidResponseException e) {
			// Invalid response
			result.setMessage("error:" + e.getMessage());
			log.error("Receive invalid response", e);
		} catch (NegativeResponseException e) {
			// Receiving negative response (non-zero command_status)
			result.setMessage("error:" + e.getMessage());
			log.error("Receive negative response, e");
		} catch (IOException e) {
			result.setMessage("error:" + e.getMessage());
			log.error("IO error occurred", e);
		}

		// remove for performance
		/*
		 * JSONObject jsonResponse = new JSONObject(); jsonResponse.put("data",
		 * result.getData()); jsonResponse.put("message", result.getMessage());
		 * jsonResponse.put("status", result.getStatus()); jsonResponse.put("messageId",
		 * result.getMessageId()); jsonResponse.put("trxId", result.getTrxId()); //resp
		 * = jsonResponse.toString();
		 */

		// insertDB
		resp = result.getMessage() + "|" + result.getStatus() + "|" + result.getMessageId();
		boolean respDb = Utils.Insert(from, to, messageId, internalId, result.getMessage());
		Utils.writeTDR(start, "MT", request.ip(), from, to, internalId, result.getStatus(), result.getMessageId(),
				result.getMessage(), "respDB:" + Boolean.toString(respDb));
		return resp;
	}

	public static Map<String, List<String>> getQueryParams(String url) {
		try {
			Map<String, List<String>> params = new HashMap<String, List<String>>();
			String[] urlParts = url.split("\\?");
			if (urlParts.length > 1) {
				String query = urlParts[1];
				for (String param : query.split("&")) {
					String[] pair = param.split("=");
					String key = URLDecoder.decode(pair[0], "UTF-8");
					String value = "";
					if (pair.length > 1) {
						value = URLDecoder.decode(pair[1], "UTF-8");
					}

					List<String> values = params.get(key);
					if (values == null) {
						values = new ArrayList<String>();
						params.put(key, values);
					}
					values.add(value);
				}
			}

			return params;
		} catch (Exception ex) {
			throw new AssertionError(ex);
		}
	}

}