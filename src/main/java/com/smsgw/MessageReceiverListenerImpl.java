package com.smsgw;

import org.jsmpp.SMPPConstant;
import org.jsmpp.bean.AlertNotification;
import org.jsmpp.bean.DataSm;
import org.jsmpp.bean.DeliverSm;
import org.jsmpp.bean.DeliveryReceipt;
import org.jsmpp.bean.MessageType;
import org.jsmpp.extra.ProcessRequestException;
import org.jsmpp.session.DataSmResult;
import org.jsmpp.session.MessageReceiverListener;
import org.jsmpp.session.Session;
import org.jsmpp.util.InvalidDeliveryReceiptException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.smsgw.models.DLR;
import com.smsgw.utils.Utils;

import java.net.URLEncoder;

public class MessageReceiverListenerImpl implements MessageReceiverListener {

	private static final Logger logger = LoggerFactory.getLogger(MessageReceiverListenerImpl.class);
	private static final String DATASM_NOT_IMPLEMENTED = "data_sm not implemented";

	@Override
	public void onAcceptDeliverSm(DeliverSm deliverSm) throws ProcessRequestException {

		if (MessageType.SMSC_DEL_RECEIPT.containedIn(deliverSm.getEsmClass())) {
			long start = System.currentTimeMillis();
			String messageId = "";
			String respDLR = "";
			String err = "";
			String url="";
			String status="";
			DLR dlr;
			
			try {
				// Receiving delivery receipt for message '63C1D697' from 628118003585 to 1616:
				// id:1673647767
				// sub:001 dlvrd:001 submit date:2104011338 done date:2104011338 stat:DELIVRD
				// err:null text:jSMPP simplify SMPP
				/* url = App.urlDLR + "&from=" + deliverSm.getSourceAddr() + "&to=" + deliverSm.getDestAddress()
						+ "&msgId=" + messageId + "&status=" + delReceipt.getFinalStatus().toString() + "&submitDate="
						+ Utils.dateFormatSimple.format(delReceipt.getSubmitDate());; */
				

				DeliveryReceipt delReceipt = deliverSm.getShortMessageAsDeliveryReceipt();
				long id = Long.parseLong(delReceipt.getId()) & 0xffffffff;
				messageId = Long.toString(id, 16).toUpperCase();
				status= delReceipt.getFinalStatus().toString();
				logger.debug("Receiving delivery receipt for message '{}' from {} to {}: {}", messageId,
						deliverSm.getSourceAddr(), deliverSm.getDestAddress(), delReceipt);

				dlr=Utils.Select(deliverSm.getSourceAddr(), messageId);

				
				String semen = delReceipt.getFinalStatus().toString();
				if(semen.equals("DELIVRD")){
					semen = "1";
				}else{
					semen = "2";
				}
				String encoded = "";
				try{
					encoded = URLEncoder.encode(delReceipt.toString(), "UTF-8" );
				}catch(Exception e){
					
				}
				//hardcoded url TODO replace from dlr.getURL but impact to performance
				url = App.urlDLR + "Pkecil=" + deliverSm.getSourceAddr() + "&Pbesar=" + deliverSm.getDestAddress()+ "&trx_id=" + messageId + "&status=" + semen + "&internalid=" + dlr.getInternalId() + "&err=" + encoded + "&fin=1";
			
				/*url = App.urlDLR + "&from=" + from + "&to=" + deliverSm.getDestAddress()
						+ "&msgId=" + messageId + "&status=" + status + "&submitDate="
						+ Utils.dateFormatSimple.format(delReceipt.getSubmitDate())+"&trxid="+trxId;
				*/
				err = delReceipt.getFinalStatus().toString();
				
			} catch (InvalidDeliveryReceiptException e) {
				logger.error("Failed getting delivery receipt", e);
				err = e.getMessage();
			}
			try {
				respDLR = Utils.getUrl(url, "");
				
			}catch (Exception e) {
				logger.error("Failed getting delivery receipt", e);
				err ="error call dlr"+e.getMessage();
			}
			//String to, String messageId, String internalId, String dlrStatus, String dlrURL,String dlrResp
			Utils.Update(deliverSm.getSourceAddr(), messageId, status, url,respDLR);
			
			Utils.writeTDR(start, "DLR", deliverSm.getId(), deliverSm.getSourceAddr(), deliverSm.getDestAddress(),
					messageId, respDLR, messageId, err, url);

		}
	}

	@Override
	public void onAcceptAlertNotification(AlertNotification alertNotification) {
		logger.info("AlertNotification not implemented");
	}

	@Override
	public DataSmResult onAcceptDataSm(DataSm dataSm, Session source) throws ProcessRequestException {
		logger.info("DataSm not implemented");
		throw new ProcessRequestException(DATASM_NOT_IMPLEMENTED, SMPPConstant.STAT_ESME_RINVCMDID);
	}

}
