package com.smsgw.utils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLContext;

import org.apache.http.client.HttpClient;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.smsgw.App;
import com.smsgw.db.MysqlFacade;
import com.smsgw.models.DLR;
import com.smsgw.routers.Router;

import spark.Request;

public class Utils {

	public static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
	public static DateFormat dateFormatSimple = new SimpleDateFormat("yyyyMMddHHmmss");
	private final static Logger log = LogManager.getLogger(Utils.class);

	public static void main(String[] args) throws UnsupportedEncodingException {
		// TODO Auto-generated method stub
		String key = "XXXXXXXX";
		String pin = "222222";
		String encrypt = encryptDES(key, pin);
		System.out.println("pin:" + pin);
		System.out.println("encrypt:" + encrypt);

		String decrypt = decryptDES(key, encrypt);
		System.out.println("decrypt:" + decrypt);

	}

	public Utils() {
	}

	public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
		Map<String, Object> retMap = new HashMap<String, Object>();

		if (json != JSONObject.NULL) {
			retMap = toMap(json);
		}
		return retMap;
	}

	public static Map<String, Object> toMap(JSONObject object) throws JSONException {
		Map<String, Object> map = new HashMap<String, Object>();

		Iterator<String> keysItr = object.keys();
		while (keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = object.get(key);

			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}

	public static List<Object> toList(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}

	public static String getUrl(String url, String body) {
		log.debug("url:" + url);
		if (url.startsWith("https"))
			return postUrls(url, body);
		String resp = "FAILED";
		HttpResponse<String> response = null;
		try {
			response = Unirest.get(url).asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resp = response.getBody();
		return resp;
	}

	public static String getUrls(String url, String body) {
		// Unirest.setHttpClient(makeClient());
		HttpClient unsafeClient = getClient();
		Unirest.setHttpClient(unsafeClient);

		String resp = "FAILED";
		HttpResponse<String> response = null;
		try {
			response = Unirest.get(url).asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resp = response.getBody();
		return resp;
	}

	public static String postUrl(String url, String body) {

		log.debug("url:" + url);
		if (url.startsWith("https"))
			return postUrls(url, body);
		String resp = "FAILED";
		HttpResponse<String> response = null;
		try {
			response = Unirest.post(url).header("Content-Type", "application/json").body(body).asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resp = response.getBody();
		return resp;
	}

	public static String postUrls(String url, String body) {
		// Unirest.setHttpClient(makeClient());
		HttpClient unsafeClient = getClient();
		Unirest.setHttpClient(unsafeClient);

		String resp = "FAILED";
		HttpResponse<String> response = null;
		try {
			response = Unirest.post(url).header("Content-Type", "application/json").body(body).asString();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resp = response.getBody();
		return resp;
	}

	private static long counter = 10000;

	public static String generateTxid(String prefix, String msisdn) {
		SimpleDateFormat sdfTxid = new SimpleDateFormat("yyMMddHHmmss");
		String dt = sdfTxid.format(new Date());
		String suffix = msisdn.substring(msisdn.length() - 5, msisdn.length());
		synchronized (sdfTxid) {
			counter++;
			if (counter > 99999) {
				counter = 10000;
			}
		}
		return prefix + dt + suffix + String.valueOf(counter);
	}

	private static String transformationDES = "DESede/ECB/PKCS5Padding";
	private static SecretKeySpec secretKey;

	public static String encryptDES(String key, String str) throws UnsupportedEncodingException {
		String res = "";
		byte[] keyBytes = Base64.getDecoder().decode(key.getBytes(StandardCharsets.UTF_8));
		secretKey = new SecretKeySpec(keyBytes, "DESede");

		byte[] bytesenc = str.getBytes();
		byte[] bytes = encryptDes(bytesenc, secretKey);
		byte[] bytedecode = Base64.getEncoder().encode(bytes);
		res = new String(bytedecode, "UTF-8");

		return res;
	}

	public static String decryptDES(String key, String str) {
		String res = "";
		byte[] keyBytes = Base64.getDecoder().decode(key.getBytes(StandardCharsets.UTF_8));
		secretKey = new SecretKeySpec(keyBytes, "DESede");

		try {
			byte[] bytesenc = Base64.getDecoder().decode(str);
			byte[] bytes = decryptDes(bytesenc, secretKey);
			res = new String(bytes, "UTF-8");

		} catch (Exception e) {
			// TODO: handle exception
			// LogMain.error(e.getMessage());
		}
		return res;
	}

	public static byte[] encryptDes(byte[] raw, SecretKeySpec secretKey) throws CipherOperationException {
		try {
			Cipher cipher = Cipher.getInstance(transformationDES);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			return cipher.doFinal(raw);
		} catch (Exception e) {
			throw new CipherOperationException(e);
		}
	}

	public static byte[] decryptDes(byte[] encrypted, SecretKeySpec secretKey) throws CipherOperationException {

		try {
			Cipher cipher = Cipher.getInstance(transformationDES);
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			return cipher.doFinal(encrypted);
		} catch (Exception e) {
			throw new CipherOperationException(e);
		}
	}

	public static HttpClient makeClient() {
		SSLContextBuilder builder = new SSLContextBuilder();
		CloseableHttpClient httpclient = null;
		try {
			// builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
			builder.loadTrustMaterial(null, new TrustStrategy() {
				@Override
				public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
					// TODO Auto-generated method stub
					return true;
				}
			});
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());

			httpclient = HttpClients.custom().setSSLSocketFactory(sslsf)
					.setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
			// System.out.println("custom httpclient called");
			// System.out.println(httpclient);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		return httpclient;
	}

	private static HttpClient unsafeHttpClient;
	private static ResultSet rs;
	static {
		try {
			SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy() {
				public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
					return true;
				}
			}).build();

			unsafeHttpClient = HttpClients.custom().setSSLContext(sslContext)
					.setSSLHostnameVerifier(new NoopHostnameVerifier()).build();

		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			e.printStackTrace();
		}
	}

	public static HttpClient getClient() {
		return unsafeHttpClient;
	}

	public static void writeTDR(long start, String type, String ip, String from, String to, String trxid, String resp, String messageId, String err, String info) {
		HashMap<String, Object> logData = new HashMap<String, Object>();
		long rt = System.currentTimeMillis() - start;
		Object logDataJson = "";
		try {
			logData.put("logTime", dateFormat.format(new Date()));
			logData.put("app", App.serverID);
			logData.put("srcIP", ip);
			logData.put("rt", rt);
			logData.put("type",type );
			logData.put("from", from);
			logData.put("to", to);
			logData.put("trxid", trxid);
			//logData.put("trxid", "aaa");
			logData.put("resp", resp);
			logData.put("messageId", messageId);
			logData.put("error", err);
			logData.put("info",info);
			
			
			ObjectMapper objectMapper = new ObjectMapper();
			logDataJson = objectMapper.writeValueAsString(logData);

		} catch (JsonProcessingException e) {
			e.printStackTrace();
			logData.put("err", e.getMessage());

		}
		String tdr = logDataJson.toString();
		LogTDR.info(tdr);

	}

	public static void writeTDRx(long start, Request request, String result, String err) {

		HashMap<String, Object> logData = new HashMap<String, Object>();
		HashMap<String, Object> addData = new HashMap<String, Object>();
		Object logDataJson = "";

		try {
			HashMap<String, Object> headers = new HashMap<>();
			for (String s : request.headers()) {
				headers.put(s, request.headers(s));
			}
			long rt = System.currentTimeMillis() - start;
			logData = new HashMap<String, Object>();
			addData = new HashMap<String, Object>();
			logData.put("logTime", dateFormat.format(new Date()));
			logData.put("app", App.serverID);
			logData.put("ver", App.serverVersion);
			logData.put("port", App.serverPort);
			logData.put("srcIP", request.ip());
			logData.put("rt", rt);
			logData.put("path", request.pathInfo());
			logData.put("header", headers);

			// get body
			String bodyString = request.body().replaceAll("\n", "").replaceAll("\t", "");

			logData.put("req", bodyString);
			logData.put("resp", result);
			logData.put("err", err);

			logData.put("addData", addData);
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
			logDataJson = objectMapper.writeValueAsString(logData);

		} catch (JsonProcessingException e) {
			e.printStackTrace();
			logData.put("err", e.getMessage());

		}
		String tdr = logDataJson.toString();
		LogTDR.info(tdr);

	}

	
	public static boolean Insert(String from, String to, String messageId, String internalId, String messageAck)  {

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String ds = "smsgw";
		String shard=to.substring(to.length() - 1);
		
		String sql = "INSERT INTO dlr"+shard+" (`from`,`to`,`message_id`,`internal_id`,`message_ack`) VALUES (?,?,?,?,?)";
		boolean result=false;
		
		try {
			log.debug("DS = " + ds + "=" + sql);
			conn = MysqlFacade.getConnection(ds);
			if (conn == null) {
				throw new SQLException("Cannot get connection from datasource : " + ds);
			}
			ps = conn.prepareStatement(sql);
			ps.setString(1, from);
			ps.setString(2, to);
			ps.setString(3, messageId);
			ps.setString(4, internalId);
			ps.setString(5, messageAck);
					
			result=ps.execute();
			
		} catch (Exception e) {
			log.error("SQL sql error = " + sql + ", ds = " + ds, e);
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					log.error("SQL sql error-rs = " + sql + ", ds = " + ds, e);

				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					log.error("SQL sql error-ps = " + sql + ", ds = " + ds, e);

				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					log.error("SQL sql error-conn = " + sql + ", ds = " + ds, e);

				}
			}
		}
		return result;
	}
	
	public static boolean Update(String to, String messageId, String dlrStatus, String dlrURL,String dlrResp)  {

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String ds = "smsgw";
		String shard=to.substring(to.length() - 1);
		String sql = "UPDATE dlr"+shard+" set dlr_status=?, dlr_url=?,  dlr_resp=?, updated_at=NOW() where `to`=? and message_id=?";
		boolean result=false;
		
		try {
			log.debug("DS = " + ds + "=" + sql);
			conn = MysqlFacade.getConnection(ds);
			if (conn == null) {
				throw new SQLException("Cannot get connection from datasource : " + ds);
			}
			ps = conn.prepareStatement(sql);
			ps.setString(1, dlrStatus);
			ps.setString(2, dlrURL);
			ps.setString(3, dlrResp);
			ps.setString(4, to);
			ps.setString(5, messageId);
			result=ps.execute();
			
		} catch (Exception e) {
			log.error("SQL sql error = " + sql + ", ds = " + ds, e);
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					log.error("SQL sql error-rs = " + sql + ", ds = " + ds, e);

				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					log.error("SQL sql error-ps = " + sql + ", ds = " + ds, e);

				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					log.error("SQL sql error-conn = " + sql + ", ds = " + ds, e);

				}
			}
		}
		return result;
	}
	
	public static DLR Select(String to, String messageId)  {
		
		DLR dlr=new DLR();
		Connection conn = null;
		PreparedStatement ps = null;
		rs = null;
		String ds = "smsgw";
		String shard=to.substring(to.length() - 1);
		
		String sql = "Select internal_id from dlr"+shard+" where `to`=? and message_id=? limit 1";
		
		try {
			log.debug("DS = " + ds + "=" + sql);
			conn = MysqlFacade.getConnection(ds);
			if (conn == null) {
				throw new SQLException("Cannot get connection from datasource : " + ds);
			}
			ps = conn.prepareStatement(sql);
			ps.setString(1, to);
			ps.setString(2, messageId);
			
			ps.executeQuery();
			if(rs.next()){
				dlr.setInternalId(rs.getString("internal_id"));
				dlr.setUrl(rs.getString("dlr_url"));
				
			}
			
		} catch (Exception e) {
			log.error("SQL sql error = " + sql + ", ds = " + ds, e);
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					log.error("SQL sql error-rs = " + sql + ", ds = " + ds, e);

				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					log.error("SQL sql error-ps = " + sql + ", ds = " + ds, e);

				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					log.error("SQL sql error-conn = " + sql + ", ds = " + ds, e);

				}
			}
		}
		return dlr;
	}
}
