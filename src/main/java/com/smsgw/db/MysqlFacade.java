package com.smsgw.db;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.sql.DataSource;

/*import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
*/
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



import com.mchange.v2.c3p0.DataSources;
import com.smsgw.App;

/*
 * - A connection facade to MySQL Database
 * - Utilize apache tomcat datasource connection, or it also can be used as standalone
 * - Use dbutil package (from apache commons) to simplify query execution
 */

public class MysqlFacade {
	
	private DataSource ds;
	private static MysqlFacade instance;
	private int connectionTimeout = 4000;
	private static Object lock = new Object();
	private static HashMap<String, MysqlFacade> instances = new HashMap<String, MysqlFacade>();
	private DataSource cp;
	private final static Logger log = LogManager.getLogger(MysqlFacade.class);
	
	private static String sampleds="jdbc:mysql://xxxx:3306/xxxx|root|null;5;5;3;12000";
	
	private MysqlFacade(String myds) throws SQLException {
		
		//get datasource
		String ds = App.prop.getProperty("ds."+myds,"undef");
		log.debug("get-datasource:"+myds+"=>"+ds);
		System.out.println("================================================"+ds);
			
		if(ds.equals("undef"))
			throw new SQLException("not found datasource");
		
		
		//start connect
		int maxPool = 15;
		int maxConn = 30;
		int initial = 5;
		int expiry = 10000;
		String driver="com.mysql.cj.jdbc.Driver";
		
		String[] p = ds.split(";");
		try {
			maxPool = Integer.parseInt(p[1]);
			maxConn = Integer.parseInt(p[2]);
			initial = Integer.parseInt(p[3]);
			expiry = Integer.parseInt(p[4]);
			
			
		} catch (Exception e1) {
			log.debug("MysqlFacade|"+ ds + "maxPool / maxConn / initial / expiry not found,using default values");
		}

		p = p[0].split("\\|");
		//driver
		driver = p[3];
	
		try {
			Class.forName(driver);
			
		} catch (ClassNotFoundException e1) {
			log.error("Error getting database driver : "+ "com.mysql.jdbc.Driver");
		}
		
		
		if(p[2].equals("null"))p[2]="";
		DataSource unpooled = DataSources.unpooledDataSource(p[0], p[1], p[2]);
		
		Map overrides = new HashMap();
		overrides.put("maxPoolSize", maxConn); // "boxed primitives" also work
		overrides.put("initialPoolSize", initial);
		overrides.put("maxIdleTime", (expiry / 1000));
		overrides.put("maxIdleTimeExcessConnections", (expiry / 1000));
		
		cp = DataSources.pooledDataSource(unpooled, overrides);

		log.info("MysqlFacade|" + ds + " pool set-up, URL = " + p[0]
				+ ", maxPool = " + maxPool + ", maxConn = " + maxConn
				+ ", initial = " + initial + ", expiry = " + expiry);
	}

	public static void resetConnection(String ds) {
		instances.remove(ds);
		log.info("MysqlFacade|ReloadingDatasource|" + ds);
	}

	public static Connection getConnection(String ds) throws Exception {
		MysqlFacade instance = instances.get(ds);
		if (instance == null) {
			synchronized (lock) {
				if (instances.get(ds) == null) {
					instance = new MysqlFacade(ds);
					instances.put(ds, instance);
				} else {
					instance = instances.get(ds);
				}
			}
		}
		if (instance.cp == null) {
			log.warn("getConnection() " + ds + " = null");
			return null;
		}
		long ts = System.currentTimeMillis();
		Connection conn = instance.cp.getConnection();
		log.debug("Thread " + Thread.currentThread().getId()
				+ " getConnection() " + ds + " = "
				+ (System.currentTimeMillis() - ts) + " ms");
		return conn;
	}



}
