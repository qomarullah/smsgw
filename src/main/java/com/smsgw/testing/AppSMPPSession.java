package com.smsgw.testing;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsmpp.bean.BindType;
import org.jsmpp.bean.NumberingPlanIndicator;
import org.jsmpp.bean.TypeOfNumber;
import org.jsmpp.session.BindParameter;
import org.jsmpp.session.SMPPSession;

import com.smsgw.App;
import com.smsgw.MessageReceiverListenerImpl;
import com.smsgw.routers.Router;

public class AppSMPPSession {

	private final static Logger log = LogManager.getLogger(Router.class);
	private SMPPSession session;
	private static AppSMPPSession INSTANCE; 
	private String info = "Initial info class";
	    
	    private AppSMPPSession() {    
	    	
	    	 BindType bindType=BindType.BIND_TX;
	    	 if (App.smppBindingType.equals("TRX")) {
	    		 bindType=BindType.BIND_TRX;
	    	 }
	    	 if (App.smppBindingType.equals("RX")) {
	    		 bindType=BindType.BIND_RX;
	    	 }
	    	 
	    	 log.debug("connect:"+App.smppServer+":"+App.smppSystemID+":"+App.smppPassword);
	    	 session = new SMPPSession();
	         try {
	             session.setMessageReceiverListener(new MessageReceiverListenerImpl());
	             String systemId = session.connectAndBind(App.smppServer, Integer.valueOf(App.smppPort), new BindParameter(bindType, App.smppSystemID, App.smppPassword, "cp", TypeOfNumber.UNKNOWN, NumberingPlanIndicator.UNKNOWN, null));
	             log.info("Connected with SMPP with system id {}", systemId);
	         } catch (IOException e) {
	        	 log.error("I/O error occured", e);
	             session = null;
	         }
	         session.setMessageReceiverListener(new MessageReceiverListenerImpl());
	            
	         	    	
	    }
	    
	    public static SMPPSession getInstance() {

	    	if(INSTANCE == null || INSTANCE.session.getSessionId()==null) {
	            INSTANCE = new AppSMPPSession();
	        }
	        
	        return INSTANCE.session;
	    }
}
