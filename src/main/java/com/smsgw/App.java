package com.smsgw;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsmpp.bean.BindType;
import org.jsmpp.bean.NumberingPlanIndicator;
import org.jsmpp.bean.TypeOfNumber;
import org.jsmpp.examples.gateway.Gateway;
import org.jsmpp.session.BindParameter;
import org.jsmpp.session.MessageReceiverListener;
import org.jsmpp.session.SMPPSession;

import com.mashape.unirest.http.Unirest;
import com.smsgw.routers.Router;
import com.smsgw.utils.Configs;

public class App {

	public static Properties prop;
	public static String urlDLR;
	public static String serverID;
	public static String serverVersion;
	public static String serverPort;
	public static int maxPool = 200;
	private final static Logger log = LogManager.getLogger(App.class);

	public static String smppServer;
	public static String smppPort;
	public static String smppSystemID;
	public static String smppPassword;
	public static String smppBindingType;

	public static void main(String[] args) {
		// setup properties
		prop = new Properties();
		if (args.length > 0)
			loadProperties(prop, args[0]);
		else {
			log.debug("properties not found");
			System.exit(0);
		}

		// setup log
		if (args.length > 1)
			BasicConfigurator.configure();
		else
			PropertyConfigurator.configure(args[0]);

		
		// setup server
		int port = Integer.parseInt(prop.getProperty("server.port", "9001"));
		int maxThreads = Integer.parseInt(prop.getProperty("server.maxthread", "200"));
		int minThreads = Integer.parseInt(prop.getProperty("server.minthread", "30"));
		int timeOutMillis = Integer.parseInt(prop.getProperty("server.timeout", "20000"));
		serverID = prop.getProperty("server.id", "ag1");
		serverVersion = prop.getProperty("server.version", "1.0");
		serverPort = Integer.toString(port);

		// smpp
		smppServer = prop.getProperty("smpp.server", "localhost");
		smppPort = prop.getProperty("smpp.port", "8056");
		smppSystemID = prop.getProperty("smpp.systemid", "j");
		smppPassword = prop.getProperty("smpp.password", "jpwd");
		smppBindingType = prop.getProperty("smpp.bindingtype", "TX");

		urlDLR = prop.getProperty("dlr.url");
		maxThreads = Integer.parseInt(prop.getProperty("dlr.maxthread", "200"));
		minThreads = Integer.parseInt(prop.getProperty("dlr.minthread", "30"));
		Unirest.setConcurrency(maxThreads, minThreads);

		// start smpp binding
		Gateway gateway = null;
		MessageReceiverListener messageReceiverListener=new MessageReceiverListenerImpl();
		 
		try {
			gateway = new AutoReconnectGateway(smppServer, Integer.parseInt(smppPort), new BindParameter(BindType.BIND_TRX, smppSystemID, smppPassword,
					"sms", TypeOfNumber.UNKNOWN, NumberingPlanIndicator.ISDN, "8080"),messageReceiverListener);

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.exit(1);
		}

		// disable jetty log
		System.setProperty("org.eclipse.jetty.util.log.class", "org.eclipse.jetty.util.log.StdErrLog");
		System.setProperty("org.eclipse.jetty.LEVEL", "OFF");

		// start server
		new Router(port, minThreads, maxThreads, timeOutMillis, gateway);
	}

	public static void loadProperties(Properties prop, String filename) {

		InputStream input = null;
		try {
			File tempFile = new File(filename);
			boolean exists = tempFile.exists();
			if (!exists) {
				System.out.println("config not found");
				System.exit(0);
			}

			input = new FileInputStream(filename);
			// load a properties file
			prop.load(input);
		} catch (IOException ex) {

			ex.printStackTrace();
		} finally {

			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
