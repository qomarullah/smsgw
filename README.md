# SMSGW

http to/from smpp using jsmpp



## Configuration

app under conf/app{port}.prop

```python
server.id=smsgw1
server.port=25001
server.minthread=10
server.maxthread=200
server.timeout=10000

smpp.server=localhost
smpp.port=8056
smpp.systemid=j
smpp.password=jpwd
smpp.bindingtype=TX

dlr.minthread=10
dlr.maxthread=200
dlr.url=http://localhost/dlr/?a=1
```

log under conf/log{port}.xml 

```python
<?xml version="1.0" encoding="UTF-8"?>
<Configuration>
	<Properties>
		<Property name="syslog">c:\\Users\\qomarullah\\eclipse-workspace2\\smsgw\\log\\syslog</Property>
		<Property name="tdrlog">c:\\Users\\qomarullah\\eclipse-workspace2\\smsgw\\log\\tdr</Property>
	</Properties>
	<Appenders>
...
```	

## SMPP Simulator
```python
java -Xmn256m -Xmx256m -verbose:gc -cp smsgw-1.0.jar com.smsgw.testing/SMPPServerSimulator
```

## How To Run

```python
./stop.sh {port} # port same as config file name
./start.sh {port} # port same as config file name

```
or 

```python
java -Xmn256m -Xmx256m -verbose:gc -Dlog4j.congurationFile=conf/log25001.xml -cp smsgw-1.0.jar com.smsgw.App conf/app25001.conf
```

sample call 

```python
http://localhost:25001/sms/submit?from=1616&to=628118003585
```

response

```python
{"messageId":"1cde2a05","message":"Success","status":"OK"}
```

delivery call

```python
http://localhost/dlr/?a=1&from=628118003585&to=1616&msgId=1CDE2A05&status=DELIVRD&submitDate=20210401163100
```

## Benchmark
```
```

## Log
trx
```python
{"app":"smsgw1","srcIP":"0:0:0:0:0:0:0:1","rt":41,"resp":"OK","messageId":"1cde2a05","from":"1616","to":"628118003585","type":"MT","error":"Success","trxid":"MT2104011631390358510001","logTime":"2021-04-01T16:31:39.289+07:00","info":""}
url:http://localhost/dlr/?a=1&from=628118003585&to=1616&msgId=1CDE2A05&status=DELIVRD&submitDate=20210401163100
```
dlr
```python
{"app":"smsgw1","srcIP":null,"rt":128,"resp":"OK","messageId":"1CDE2A05","from":"628118003585","to":"1616","type":"DLR","error":"DELIVRD","trxid":"1CDE2A05","logTime":"2021-04-01T16:31:40.422+07:00","info":"http://localhost/dlr/?a=1&from=628118003585&to=1616&msgId=1CDE2A05&status=DELIVRD&submitDate=20210401163100"}
```


## Build
Use direct from IDE or maven to build  fat jar 

```python
mvn clean install
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)